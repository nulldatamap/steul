use bitflags::bitflags;
use std::alloc::{alloc, dealloc, Layout};
use std::cell::RefCell;
use std::marker::PhantomData;
use std::mem::size_of;
use std::pin::{pin, Pin};
use std::ptr::NonNull;

const HEAP_DEFAULT_SIZE: usize = 8 * 1024; // 8K, smol
const HEAP_ALIGN: usize = 16;

pub trait GcRef<T> {
    unsafe fn as_ptr(&self) -> NonNull<T>;

    fn borrow<'heap>(&self, h: &'heap impl Heaplike) -> &'heap T {
        unsafe { h.get_heap().borrow_from_ptr(self.as_ptr()) }
    }

    // NOTE: we're returning an exclusive reference even though
    // we're getting it from a non-exclusively referenced Gc value.
    // This is because the exclusiveness is enforced via the heap
    fn borrow_mut<'heap>(&self, h: &'heap mut impl Heaplike) -> &'heap mut T {
        unsafe { h.get_heap_mut().borrow_mut_from_ptr(self.as_ptr()) }
    }
}

#[derive(Debug)]
pub struct Gc<'root, T: 'root> {
    ptr: NonNull<T>,
    _phantom: PhantomData<&'root Heap>,
}

impl<'root, T> GcRef<T> for Gc<'root, T> {
    unsafe fn as_ptr(&self) -> NonNull<T> {
        self.ptr
    }
}

bitflags! {
    #[derive(Copy, Clone, PartialEq, Eq)]
    pub struct ObjectFlags: u32 {
        const NONE = 0b000;
        const FWD_PTR = 0b001;
    }
}

// Guarrantee that data is that the end of the struct
#[repr(align(16), C)]
struct Header {
    flags: ObjectFlags,
    len: u32,
    data: [usize; 0],
}

impl Header {
    fn len(&self) -> usize {
        assert!(!self.is_fwd_ptr());
        self.len as usize
    }

    fn is_fwd_ptr(&self) -> bool {
        self.flags.contains(ObjectFlags::FWD_PTR)
    }

    // TODO: We're returning a bare Gc, which is VERY unsafe in theory
    // This should only be called inside the gc routine, so it isn't so
    // bad, but maybe it should really return an `Unrooted` instead.
    unsafe fn as_fwd_ptr<'to>(&self, to_h: &'to Heap) -> Option<Gc<'to, Object<'to>>> {
        if self.is_fwd_ptr() {
            let raw = ((self.len as usize) << 32) | (self.flags.bits() as usize & !1);
            assert!(raw != 0);
            let gc = Gc {
                ptr: NonNull::new_unchecked(raw as *mut Object<'to>),
                _phantom: PhantomData,
            };
            assert!(to_h.points_to_inside_heap(gc.ptr));
            Some(gc)
        } else {
            None
        }
    }

    unsafe fn become_fwd_ptr_to<'to>(&mut self, ptr: NonNull<Object<'to>>) {
        let ptr_bits = ptr.as_ptr() as usize;
        self.flags =
            ObjectFlags::from_bits_retain((ptr_bits & 0xFFFF_FFFF) as u32) | ObjectFlags::FWD_PTR;
        self.len = (ptr_bits >> 32) as u32;
    }
}

pub struct Object<'root> {
    header: Header,
    _phantom: PhantomData<&'root ()>,
}

impl<'root> Object<'root> {
    pub fn slot_count(&self) -> usize {
        self.header.len()
    }

    pub fn get(&self, i: usize) -> &Val<'root> {
        self.try_get(i).expect("slot index out of bounds")
    }

    pub fn get_mut(&mut self, i: usize) -> &mut Val<'root> {
        self.try_get_mut(i).expect("slot index out of bounds")
    }

    pub fn set<'other>(&mut self, i: usize, val: &impl ValStore<'other>) {
        val.store_to_val(self.get_mut(i));
    }

    pub fn try_get(&self, i: usize) -> Option<&Val<'root>> {
        if i < self.slot_count() {
            unsafe { Some(self.unchecked_get(i)) }
        } else {
            None
        }
    }

    pub fn try_get_mut(&mut self, i: usize) -> Option<&mut Val<'root>> {
        if i < self.slot_count() {
            unsafe { Some(self.unchecked_get_mut(i)) }
        } else {
            None
        }
    }

    unsafe fn unchecked_get(&self, i: usize) -> &Val<'root> {
        assert!(!self.header.is_fwd_ptr());
        let data_ptr = &self.header.data as *const usize as *const Val<'root>;
        data_ptr.add(i).as_ref::<'root>().unwrap()
    }

    unsafe fn unchecked_get_mut(&mut self, i: usize) -> &mut Val<'root> {
        assert!(!self.header.is_fwd_ptr());
        let data_ptr = &mut self.header.data as *mut usize as *mut Val<'root>;
        data_ptr.add(i).as_mut::<'root>().unwrap()
    }
}

impl<'root> std::fmt::Debug for Object<'root> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "<object@{:?}[{}]", self as *const Self, self.header.len)?;
        for i in 0..self.header.len as usize {
            write!(f, " {:016X}", self.get(i).raw)?;
        }
        write!(f, ">")
    }
}

pub unsafe trait Trace {
    fn trace<'heap>(&mut self, gc: &mut Collector<'heap>);
}

unsafe impl<'root> Trace for Object<'root> {
    fn trace<'heap>(&mut self, gc: &mut Collector<'heap>) {
        for i in 0..self.slot_count() {
            gc.edge(self.get_mut(i));
        }
    }
}

const VAL_TAG_MASK: usize = 0b111;
const VAL_INT_TAG: usize = 0b001;
const VAL_OBJ_TAG: usize = 0b000;
const VAL_INT_MASK: usize = 0b001;
const VAL_INT_SHIFT: usize = 1;

pub struct Val<'root> {
    raw: usize,
    _phantom: PhantomData<&'root ()>,
}

impl<'root> Val<'root> {
    pub fn as_obj(&'root self) -> Option<&'root Gc<'root, Object<'root>>> {
        if self.raw == 0 {
            return None;
        }

        if (self.raw & VAL_TAG_MASK) == VAL_OBJ_TAG {
            Some(unsafe { std::mem::transmute(self) })
        } else {
            None
        }
    }

    pub fn as_value(&'root self) -> Value<'root> {
        if self.raw == 0 {
            return Value::Nil;
        }

        if (self.raw & VAL_TAG_MASK) == VAL_OBJ_TAG {
            Value::Obj(unsafe { std::mem::transmute(self) })
        } else if (self.raw & VAL_INT_MASK) == VAL_INT_TAG {
            Value::Int((self.raw as i64) >> VAL_INT_SHIFT)
        } else {
            panic!("Invalid val!")
        }
    }

    pub fn as_mut_obj(&'root mut self) -> Option<&'root mut Gc<'root, Object<'root>>> {
        if (self.raw & VAL_TAG_MASK) == VAL_OBJ_TAG {
            Some(unsafe { std::mem::transmute(self) })
        } else {
            None
        }
    }

    unsafe fn from_ptr(ptr: NonNull<Object<'_>>) -> Val<'root> {
        Val {
            raw: (ptr.as_ptr() as usize) | VAL_OBJ_TAG,
            _phantom: PhantomData,
        }
    }
}

#[derive(Copy, Clone)]
pub enum Value<'root> {
    Nil,
    Int(i64),
    Obj(&'root Gc<'root, Object<'root>>),
}

impl<'root> From<&'root Gc<'root, Object<'root>>> for Value<'root> {
    fn from(o: &'root Gc<'root, Object<'root>>) -> Value<'root> {
        Value::Obj(o)
    }
}

pub trait ValStore<'root>: 'root {
    fn store_to_val<'other>(&self, val: &'other mut Val);
}

impl<'root> ValStore<'root> for Val<'root> {
    fn store_to_val<'other>(&self, val: &'other mut Val) {
        val.raw = self.raw;
    }
}

impl<'root> ValStore<'root> for Value<'root> {
    fn store_to_val<'other>(&self, val: &'other mut Val) {
        let raw = match self {
            Value::Nil => 0,
            Value::Int(i) => ((i << 1) as usize) | VAL_INT_TAG,
            Value::Obj(o) => (o.ptr.as_ptr() as usize) | VAL_OBJ_TAG,
        };
        val.raw = raw;
    }
}

pub struct Unrooted<'heap, T> {
    inner: Gc<'heap, T>,
}

impl<'a, T> GcRef<T> for Unrooted<'a, T> {
    unsafe fn as_ptr(&self) -> NonNull<T> {
        self.inner.as_ptr()
    }
}

impl<'heap, T: 'heap + GcRef<Object<'heap>>> ValStore<'heap> for T {
    fn store_to_val<'other>(&self, val: &'other mut Val) {
        unsafe {
            *val = Val::from_ptr(self.as_ptr());
        }
    }
}

impl<'heap, T> Unrooted<'heap, T> {
    fn as_ref(&'heap self) -> &'heap Gc<'heap, T> {
        &self.inner
    }
}

struct UntypedRoot {
    root: NonNull<dyn Trace>,
}

impl UntypedRoot {
    unsafe fn get_trace(&mut self) -> &mut dyn Trace {
        self.root.as_mut()
    }
}

pub struct Root<T: Trace> {
    // Raw pointer into the heap, it's the backing address
    // of the rooted Gc<> returned when the root is used
    value: Option<NonNull<T>>,
}

impl<T: Trace> Root<T> {
    pub fn new() -> Root<T> {
        Root { value: None }
    }
}

unsafe impl<T: Trace> Trace for Root<T> {
    fn trace(&mut self, gc: &mut Collector) {
        if let Some(ptr) = self.value.as_mut() {
            gc.edge(unsafe { std::mem::transmute(ptr) });
        }
    }
}

pub struct StackRoot<'root, T: Trace> {
    root: Pin<&'root mut Root<T>>,
}

impl<'root, T: Trace> StackRoot<'root, T> {
    pub fn new(root: Pin<&'root mut Root<T>>) -> StackRoot<'root, T> {
        StackRoot { root }
    }
}

impl<'root> GcRef<Object<'root>> for StackRoot<'root, Object<'root>> {
    unsafe fn as_ptr(&self) -> NonNull<Object<'root>> {
        // Volatile since the gc pointer might have gotten patched
        // if a collection cycle occoured
        ((&*self.root) as *const Root<Object<'root>>)
            .read_volatile()
            .value
            .unwrap()
    }
}

pub fn in_root<'heap, 'root>(
    v: impl GcRef<Object<'heap>>,
    sr: &mut StackRoot<'root, Object<'root>>,
) {
    assert!(sr.root.value.is_none());
    sr.root.value = unsafe { Some(v.as_ptr().cast()) };
    let untyped_root = unsafe {
        let root_addr =
            NonNull::new_unchecked((&mut *sr.root) as *mut Root<Object<'root>> as *mut dyn Trace);
        UntypedRoot { root: root_addr }
    };
    GC_ROOTS.with(|roots| {
        roots.borrow_mut().push(untyped_root);
    });
}

impl<'root, T: Trace> Drop for StackRoot<'root, T> {
    fn drop(&mut self) {
        self.root.value = None;
        GC_ROOTS.with(|roots| {
            let dropped_root = roots.borrow_mut().pop();
            assert!(dropped_root
                .map(|x| x.root.cast().as_ptr() == &mut *self.root as *mut Root<T>)
                .unwrap_or(false));
        });
    }
}

#[macro_export]
macro_rules! letroot {
    ($name:ident = $val:expr) => {
        let __pinned_root = pin!(Root::new());
        let mut __stack_root = StackRoot::new(__pinned_root);
        in_root($val, &mut __stack_root);
        let $name = __stack_root;
    };
}

thread_local! {
    static GC_ROOTS: RefCell<Vec<UntypedRoot>> = RefCell::new(Vec::new());
}

#[derive(Default, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct StatValue {
    pub this_cycle: usize,
    pub total: usize,
}

impl StatValue {
    fn cycle(&mut self) {
        self.this_cycle = 0;
    }
}

impl std::ops::AddAssign<usize> for StatValue {
    fn add_assign(&mut self, other: usize) {
        self.this_cycle += other;
        self.total += other;
    }
}

impl std::ops::SubAssign<usize> for StatValue {
    fn sub_assign(&mut self, other: usize) {
        self.this_cycle -= other;
        self.total -= other;
    }
}

#[derive(Default, Copy, Clone, Debug)]
pub struct GcStats {
    pub cycles: usize,
    pub memory_reserved: usize,
    pub objects_allocated: StatValue,
    pub objects_moved: StatValue,
    pub objects_collected: StatValue,
    pub memory_allocated: StatValue,
    pub memory_moved: StatValue,
    pub memory_collected: StatValue,
}

impl GcStats {
    fn cycle(&mut self) {
        self.cycles += 1;
        self.objects_allocated.cycle();
        self.objects_moved.cycle();
        self.objects_collected.cycle();
        self.memory_allocated.cycle();
        self.memory_moved.cycle();
        self.memory_collected.cycle();
    }
}

pub trait Heaplike {
    fn get_heap(&self) -> &Heap;
    fn get_heap_mut(&mut self) -> &mut Heap;
}

pub struct GcHandle(usize);

#[derive(Debug)]
pub struct StaticRoot {
    ptr: NonNull<()>,
}

unsafe impl Trace for StaticRoot {
    fn trace(&mut self, gc: &mut Collector) {
        gc.edge(unsafe { std::mem::transmute(&mut self.ptr) });
    }
}

#[derive(Debug)]
pub struct StaticRootSlot {
    // TODO:
    // version: usize,
    pub root: Option<StaticRoot>,
}

pub struct GcContext {
    heap: Heap,
    pub static_roots: Vec<StaticRootSlot>,
    stats: Option<GcStats>,
}

impl Heaplike for GcContext {
    fn get_heap(&self) -> &Heap {
        &self.heap
    }

    fn get_heap_mut(&mut self) -> &mut Heap {
        &mut self.heap
    }
}

impl GcContext {
    pub fn new() -> GcContext {
        let heap = Heap::new();
        let mut stats = GcStats::default();
        stats.memory_reserved = heap.capacity;
        GcContext {
            heap,
            static_roots: Vec::new(),
            stats: Some(stats),
        }
    }

    pub fn stats(&self) -> Option<GcStats> {
        self.stats
    }

    pub fn alloc<'heap>(&'heap mut self, len: usize) -> Unrooted<'heap, Object<'heap>> {
        self.heap
            .alloc(len, self.stats.as_mut())
            .expect("out of heap memory")
    }

    pub fn alloc_gc_handle<'heap>(&mut self, x: &impl GcRef<Object<'heap>>) -> GcHandle {
        let (i, slot) = if let Some((i, slot)) = self
            .static_roots
            .iter_mut()
            .enumerate()
            .find(|(_, slot)| slot.root.is_none())
        {
            (i, slot)
        } else {
            self.static_roots.push(StaticRootSlot { root: None });
            let i = self.static_roots.len() - 1;
            (i, &mut self.static_roots[i])
        };

        let ptr;
        unsafe {
            let x_ptr = x.as_ptr();
            ptr = x_ptr.cast::<()>();
        }

        slot.root = Some(StaticRoot { ptr });

        GcHandle(i)
    }

    pub fn free_gc_handle(&mut self, h: GcHandle) {
        self.static_roots.remove(h.0);
    }

    pub fn get(&self, h: &GcHandle) -> &Gc<Object> {
        if let Some(root) = self.static_roots[h.0].root.as_ref() {
            unsafe { std::mem::transmute(&root.ptr) }
        } else {
            panic!("Invalid GcHandle!");
        }
    }

    pub fn get_mut(&mut self, h: &GcHandle) -> &mut Gc<Object> {
        if let Some(root) = self.static_roots[h.0].root.as_mut() {
            unsafe { std::mem::transmute(&mut root.ptr) }
        } else {
            panic!("Invalid GcHandle!");
        }
    }

    pub fn gc(&mut self) {
        let mut to = Heap::new();
        let mut collector = Collector {
            gc: self,
            to: &mut to,
        };
        collector.collect();
        self.heap = to;
        self.stats
            .as_mut()
            .map(|stats| stats.memory_reserved = self.heap.capacity);
    }
}

pub struct Heap {
    block: NonNull<u8>,
    idx: usize,
    capacity: usize,
}

impl Heap {
    fn layout() -> Layout {
        Layout::from_size_align(HEAP_DEFAULT_SIZE, HEAP_ALIGN)
            .expect("Misconfigured heap parameters")
    }

    fn new() -> Heap {
        let ptr: NonNull<u8>;
        unsafe {
            ptr = NonNull::new(alloc(Heap::layout())).expect("Failed to allocate heap");
        }
        Heap {
            block: ptr,
            idx: 0,
            capacity: HEAP_DEFAULT_SIZE,
        }
    }

    fn alloc<'heap>(
        &'heap mut self,
        len: usize,
        stats: Option<&mut GcStats>,
    ) -> Option<Unrooted<'heap, Object<'heap>>> {
        Some(Unrooted {
            inner: self.try_alloc(len, true, stats)?,
        })
    }
    unsafe fn borrow_from_ptr<T>(&self, p: NonNull<T>) -> &T {
        if !self.points_to_inside_heap(p) {
            panic!("Gc ptr doesn't belong to heap!");
        }
        unsafe { p.as_ref() }
    }

    unsafe fn borrow_mut_from_ptr<T>(&mut self, mut p: NonNull<T>) -> &mut T {
        if !self.points_to_inside_heap(p) {
            panic!("Gc ptr doesn't belong to heap!");
        }
        unsafe { p.as_mut() }
    }

    fn copy_from<'heap, 'from>(
        &'heap mut self,
        val: &'from Object<'from>,
        stats: Option<&mut GcStats>,
    ) -> Unrooted<'heap, Object> {
        let header = self
            .try_alloc_internal(val.slot_count(), stats)
            .expect("out of heap memory");
        unsafe {
            let object: NonNull<Object> = header.cast();
            {
                let object = object.as_ptr();
                for i in 0..(*object).slot_count() {
                    (*object).set(i, val.get(i));
                }
            }

            Unrooted {
                inner: Gc {
                    ptr: object,
                    _phantom: PhantomData::<&'heap Self>,
                },
            }
        }
    }

    fn points_to_inside_heap<T>(&self, addr: NonNull<T>) -> bool {
        let addr = addr.cast::<u8>().as_ptr();
        let diff = unsafe { addr.byte_offset_from(self.block.as_ptr()) };
        diff >= 0 && (diff as usize) < self.idx
    }

    fn try_alloc<'root>(
        &'root mut self,
        len: usize,
        init_slots: bool,
        stats: Option<&mut GcStats>,
    ) -> Option<Gc<'root, Object>> {
        let header = self.try_alloc_internal(len, stats)?;
        unsafe {
            let object: NonNull<Object> = header.cast();

            if init_slots {
                let object = object.as_ptr();
                for i in 0..(*object).slot_count() {
                    (*object).set(i, &Value::Nil);
                }
            }

            Some(Gc {
                ptr: object,
                _phantom: PhantomData::<&'root Self>,
            })
        }
    }
    fn object_end_offset(len: usize) -> usize {
        let unaligned = size_of::<Header>() + size_of::<usize>() * len;
        (unaligned + HEAP_ALIGN - 1) / HEAP_ALIGN * HEAP_ALIGN
    }

    fn try_alloc_internal(
        &mut self,
        len: usize,
        stats: Option<&mut GcStats>,
    ) -> Option<NonNull<Header>> {
        if len > u32::MAX as usize {
            panic!("`len` argument invalid, too large");
        }
        unsafe {
            let ptr = self.block.as_ptr().add(self.idx);
            let offset = Heap::object_end_offset(len);
            if offset + self.idx > self.capacity {
                None
            } else {
                stats.map(|stats| {
                    stats.objects_allocated += 1;
                    stats.memory_allocated += offset;
                });
                self.idx += offset;
                let header = ptr as *mut Header;
                (*header).flags = ObjectFlags::NONE;
                (*header).len = len as u32;
                Some(NonNull::new_unchecked(header))
            }
        }
    }
}

struct Collector<'a> {
    gc: &'a mut GcContext,
    to: &'a mut Heap,
}

impl<'a> Collector<'a> {
    fn collect(&mut self) {
        let before = self.gc.stats.as_mut().map(|stats| {
            (
                stats.objects_allocated.this_cycle,
                stats.memory_allocated.this_cycle,
            )
        });
        // Visit the roots
        GC_ROOTS.with(|roots| {
            for root in roots.borrow_mut().iter_mut() {
                let rooted_obj = unsafe { root.get_trace() };
                rooted_obj.trace(self);
            }
        });
        let mut static_roots = Vec::new();

        // We swap so we can mutable iterate through the roots while still
        // mutably borrowing the GcContext
        std::mem::swap(&mut static_roots, &mut self.gc.static_roots);
        for root in static_roots.iter_mut() {
            if let Some(root) = root.root.as_mut() {
                root.trace(self);
            }
        }
        std::mem::swap(&mut static_roots, &mut self.gc.static_roots);

        let mut head = 0;

        while head < self.to.idx {
            let obj = unsafe {
                self.to
                    .block
                    .offset(head as isize)
                    .cast::<Object>()
                    .as_mut()
            };
            let obj_len = obj.slot_count();
            obj.trace(self);
            head += Heap::object_end_offset(obj_len);
        }

        if let Some((objects_before, memory_before)) = before {
            self.gc.stats.as_mut().map(|stats| {
                let memory_moved = stats.memory_allocated.this_cycle - memory_before;
                let objects_moved = stats.objects_allocated.this_cycle - objects_before;
                stats.objects_moved += objects_moved;
                stats.memory_moved += memory_moved;

                stats.objects_collected += stats.objects_allocated.this_cycle - objects_moved;
                stats.memory_collected += stats.memory_allocated.this_cycle - memory_moved;

                // Don't count moved objects as newly allocated
                stats.objects_allocated -= objects_moved;
                stats.memory_allocated -= memory_moved;

                stats.cycle();
            });
        }
    }

    fn edge<'root>(&mut self, val: &'root mut Val) {
        if let Some(obj) = val.as_obj() {
            // Has already been visited and patched
            if self.to.points_to_inside_heap(obj.ptr) {
                return;
            }
            assert!(self.gc.heap.points_to_inside_heap(obj.ptr));
            if let Some(new_addr) = unsafe { obj.borrow(&self.gc.heap).header.as_fwd_ptr(&self.to) } {
                // The object being pointed to has been moved, so patch it
                Value::Obj(&new_addr).store_to_val(val);
            } else {
                // Hasn't been visited before, so let's move it!
                unsafe {
                    let obj_val = obj.borrow(&self.gc.heap);
                    // Create a copy on the to-heap
                    let new_obj_val = self.to.copy_from(obj_val, self.gc.stats.as_mut());
                    // Change the object on the from-heap to a FWD_PTR
                    obj.borrow_mut(&mut self.gc.heap)
                        .header
                        .become_fwd_ptr_to(new_obj_val.inner.ptr);
                    // And overwrite the current edge with the new object on the to-heap
                    Value::Obj(new_obj_val.as_ref()).store_to_val(val);
                };
            }
        }
    }
}

impl Heaplike for Heap {
    fn get_heap(&self) -> &Heap {
        self
    }
    fn get_heap_mut(&mut self) -> &mut Heap {
        self
    }
}

impl Drop for Heap {
    fn drop(&mut self) {
        unsafe {
            dealloc(self.block.as_mut(), Heap::layout());
        }
    }
}
