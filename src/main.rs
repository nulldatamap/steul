#![feature(ptr_metadata)]
#![feature(non_null_convenience)]
mod rt;

use std::pin::pin;

use rt::*;

struct Steul {
    gc: GcContext,
    root: GcHandle,
}

struct EditorApp {
    cx: Steul,
}

fn inspect_obj(gc: &GcContext, ui: &mut egui::Ui, optr: &Gc<Object>) {
    let o = optr.borrow(gc);
    egui::CollapsingHeader::new(format!("<object@{:?}>", unsafe{ optr.as_ptr() }))
        .show(ui, |ui| {
            for i in 0..o.slot_count() {
                let x = o.get(i).as_value();
                match x {
                    Value::Nil => {ui.label("nil");},
                    Value::Int(i) => {ui.label(format!("{}", i));},
                    Value::Obj(o) => inspect_obj(gc, ui, o),
                };
            }
        });
}

impl eframe::App for EditorApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        let mut should_gc = false;
        let mut garbage = false;
        let mut open = true;
        egui::Window::new("Root object inspector").open(&mut open).show(ctx, |ui| {
            should_gc = ui.button("GC!").clicked();
            garbage = ui.button("Allocate garbage").clicked();
            inspect_obj(&self.cx.gc, ui, self.cx.gc.get(&self.cx.root));
        });

        egui::Window::new("GC Roots").open(&mut open).show(ctx, |ui| {
            egui::CollapsingHeader::new("Static roots")
                .show(ui, |ui| {
                    for (i, rs) in self.cx.gc.static_roots.iter().enumerate() {
                        ui.label(format!("{} - {:?}", i, rs)) ;
                    }
                });
        });

        egui::Window::new("GC Stats").open(&mut open).show(ctx, |ui| {
            ui.label(format!("{:#?}", self.cx.gc.stats()));
        });

        if garbage {
            self.cx.gc.alloc(64);
        }

        if should_gc {
            self.cx.gc.gc();
        }
    }
}

fn main() -> eframe::Result<()> {
    let mut gc = GcContext::new();
    let root: GcHandle;
    {
        letroot!(rr = gc.alloc(5));
        letroot!(x = gc.alloc(10));
        {
            let r = rr.borrow_mut(&mut gc);
            for i in 0..4 {
                r.set(i, &Value::Int(i as i64));
            }
            r.set(3, &x);
            r.set(4, &rr);
        }

        root = gc.alloc_gc_handle(&rr);
    }

    let cx = Steul {
        gc, root
    };

    let options = eframe::NativeOptions::default();

    eframe::run_native("Steul Bootstrap Editor", options, Box::new(move |cc| {
        Box::<EditorApp>::new(EditorApp {
            cx
        })
    }))
}
